package com.boot.test.contoller;

import jdk.nashorn.internal.runtime.linker.Bootstrap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName:
 * @Description:
 * @Auther: JingFeng.Tan
 * @Date: 2019/3/7 11:35
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/booleanTest")
    public String booleanTest(boolean a) {
        return "这是一次新的构建";
    }

}
