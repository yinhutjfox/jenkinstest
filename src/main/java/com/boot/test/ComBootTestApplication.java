package com.boot.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComBootTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComBootTestApplication.class, args);
    }

}
